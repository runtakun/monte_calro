package main

import (
	"bufio"
	"math/rand"
	"os"
	"strconv"
)

func main() {

	f, err := os.Create("./monte_calro.csv")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	w := bufio.NewWriter(f)
	defer w.Flush()

	for i := 0; i < 100000000; i++ {
		w.WriteString(strconv.Itoa(i))
		w.WriteString(",")
		w.WriteString(strconv.FormatFloat(rand.Float64(), 'f', 12, 64))
		w.WriteString(",")
		w.WriteString(strconv.FormatFloat(rand.Float64(), 'f', 12, 64))
		w.WriteString("\n")
	}

}
